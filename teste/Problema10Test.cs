﻿using NUnit.Framework;
using probleme.probleme;

namespace teste
{
    class Problema10Test
    {
        [Test]
        public static void Test1()
        {
            int[,] Matrice = new int[2, 2];
            Matrice[0, 0] = 1; Matrice[0, 1] = 7;
            Matrice[1, 0] = 2; Matrice[1, 1] = 9;
            Problema10.OrdoneazaMatricia2D(2, Matrice, true);
            CollectionAssert.AreEqual(new int[,] { { 1, 2 }, { 7, 9 } }, Matrice);
        }

        [Test]
        public static void Test2()
        {
            int[,] Matrice = new int[2, 3];
            Matrice[0, 0] = 1; Matrice[0, 1] = 7; Matrice[0, 2] = 4;
            Matrice[1, 0] = 2; Matrice[1, 1] = 9; Matrice[1, 2] = 8;
            Problema10.OrdoneazaMatricia2D(3, Matrice, true);
            CollectionAssert.AreEqual(new int[,] { { 1, 2, 4 }, { 7, 8, 9 } }, Matrice);
        }

        [Test]
        public static void Test3()
        {
            int[,] Matrice = new int[3, 3];
            Matrice[0, 0] = 1; Matrice[0, 1] = 3; Matrice[0, 2] = 5; //1, 3, 5
            Matrice[1, 0] = 3; Matrice[1, 1] = 2; Matrice[1, 2] = 9; //3, 2, 9
            Matrice[2, 0] = 8; Matrice[2, 1] = 2; Matrice[2, 2] = 6; //8, 2, 6
            Problema10.SwapMatrice(3, Matrice, 0);
            CollectionAssert.AreEqual(new int[,] { { 5, 3, 1 }, { 9, 2, 3 }, { 6, 2, 8 } }, Matrice);
        }

        [Test]
        public static void Test4()
        {
            int[,] Matrice = new int[3, 3];
            Matrice[0, 0] = 1; Matrice[0, 1] = 3; Matrice[0, 2] = 5; //1, 3, 5
            Matrice[1, 0] = 3; Matrice[1, 1] = 2; Matrice[1, 2] = 9; //3, 2, 9
            Matrice[2, 0] = 8; Matrice[2, 1] = 2; Matrice[2, 2] = 6; //8, 2, 6
            Problema10.SwapMatrice(3, Matrice, 1);
            CollectionAssert.AreEqual(new int[,] { { 8, 2, 6 }, { 3, 2, 9 }, { 1, 3, 5 } }, Matrice);
        }
    }
}
