﻿using NUnit.Framework;
using probleme.probleme;

namespace teste
{
    public class Problema1Test
    {
        [Test]
        public void Test1()
        {
            int[] matrice = new int[] { 9, 1 , 3 };

            Problema1.SorteazaCrescator(matrice);

            Assert.AreEqual(3, matrice.Length);

            Assert.AreEqual(1, matrice[0]);
            Assert.AreEqual(3, matrice[1]);
            Assert.AreEqual(9, matrice[2]);
        }

        [Test]
        public void Test2()
        {
            int[] matrice = new int[] { 7, 1 };

            Problema1.SorteazaCrescator(matrice);

            Assert.AreEqual(1, matrice[0]);
            Assert.AreEqual(7, matrice[1]);
        }
    }
}
