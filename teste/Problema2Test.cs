﻿using NUnit.Framework;
using probleme.probleme;

namespace teste
{
    class Problema2Test
    {
        [Test]
        public void Test1()
        {
            int[] matricie = new int[] {5, 9, 1, 7 };
            Problema2.OrdoneazaMatricia(false, 0, 1, matricie);
            Assert.AreEqual(9, matricie[0]);
            Assert.AreEqual(5, matricie[1]);
        }

        [Test]
        public void Test2()
        {
            int[] matricie = new int[] { 3, 2, 8, 7 };
            Problema2.OrdoneazaMatricia(true, 2, 3, matricie);
            Assert.AreEqual(7, matricie[2]);
            Assert.AreEqual(8, matricie[3]);
        }

        [Test]
        public void Test3()
        {
            int[] matricie = new int[] { 10, 15, 31, 20  };
            Problema2.OrdoneazaMatricia(false, 0, 1, matricie);
            Problema2.OrdoneazaMatricia(true, 2, 3, matricie);

            //Assert.AreEqual((15, 10, 20, 31), matricie);
            CollectionAssert.AreEqual(new int[] { 15, 10, 20, 31 }, matricie);
        }
    }
}
