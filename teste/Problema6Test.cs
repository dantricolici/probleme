﻿using NUnit.Framework;
using probleme.probleme;

namespace teste
{
    class Problema6Test
    {
        [Test]
        public void Test1()
        {
            string cuvint = "cartof";
            cuvint = Problema6.InverseazaPropozitia(cuvint);
            Assert.AreEqual("fotrac", cuvint);
        }

        [Test]
        public void Test2()
        {
            string cuvint = "cinci";
            cuvint = Problema6.InverseazaPropozitia(cuvint);
            Assert.AreEqual("icnic", cuvint);
        }
    }
}
