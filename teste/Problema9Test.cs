﻿using NUnit.Framework;
using probleme.probleme;
using System;
using System.Collections.Generic;
using System.Text;

namespace teste
{
    class Problema9Test
    {
        [Test]
        public void Test1()
        {
            string[] cuvinte = new string[] {"cartof", "nu", "e", "mancare"};
            Problema9.SortareaCuvintelor(cuvinte, 0, cuvinte.Length);
            CollectionAssert.AreEqual(new string[] { "e", "nu", "cartof", "mancare" }, cuvinte);
        }

        [Test]
        public void Test2()
        {
            string[] cuvinte = new string[] { "cartof", "nu", "e", "mancare" };
            Problema9.SortareaCuvintelor(cuvinte, 1, cuvinte.Length);
            CollectionAssert.AreEqual(new string[] { "mancare", "cartof", "nu", "e" }, cuvinte);
        }
        [Test]
        public void Test3()
        {
            string[] cuvinte = new string[] { "cartof", "nu", "e", "mancare", "abcdefgh" };
            Problema9.SortareaCuvintelor(cuvinte, 1, cuvinte.Length);
            CollectionAssert.AreEqual(new string[] {"abcdefgh" ,"mancare", "cartof", "nu", "e" }, cuvinte);
        }

        [Test]
        public void Test4()
        {
            string[] cuvinte = new string[] { "cartof", "nu", "e", "mancare", "cartof" };
            Problema9.SortareaCuvintelor(cuvinte, 0, cuvinte.Length);
            CollectionAssert.AreEqual(new string[] { "e", "nu", "cartof", "cartof", "mancare" }, cuvinte);
        }
    }
}
