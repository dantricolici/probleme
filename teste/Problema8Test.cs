﻿using NUnit.Framework;
using probleme.probleme;

namespace teste
{
    class Problema8Test
    {
        [Test]
        public void Test1()
        {
            string fraz = "cac, cacati?";
            fraz = Problema8.PuneLitereMari(fraz);
            Assert.AreEqual("Cac, Cacati?", fraz);
        }
    }
}
