﻿using NUnit.Framework;
using probleme;

namespace teste
{
    class Problema7Test
    {
        [Test]
        public void Test1()
        {
            string fraz = "caca nu e caca fara caca";
            string[] cuvinte = RezolvaProblema.GasesteCuvinte(fraz);
            Assert.AreEqual("caca", cuvinte[0]);
            Assert.AreEqual("nu", cuvinte[1]);
            Assert.AreEqual("e", cuvinte[2]);
            Assert.AreEqual("caca", cuvinte[3]);
            Assert.AreEqual("fara", cuvinte[4]);
            Assert.AreEqual("caca", cuvinte[5]);
        }
    }
}
