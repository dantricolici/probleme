﻿using NUnit.Framework;
using probleme.probleme;

namespace teste
{
    class Problema3Test
    {
        [Test]
        public void Test1()
        {
            int[] matrice = new int[] { 1, 9, 4, 1 };
            Problema3.InverseazaMatricea(matrice);
            CollectionAssert.AreEqual(new int[] { 1, 4, 9, 1 }, matrice);
        }

        [Test]
        public void Test2()
        {
            int[] matrice = new int[] { 1, 0, 2};
            Problema3.InverseazaMatricea(matrice);
            CollectionAssert.AreEqual(new int[] { 2, 0, 1}, matrice);
        }
    }
}
