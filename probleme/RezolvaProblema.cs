﻿using probleme.probleme;
using System;
using System.Collections.Generic;
using System.Text;

namespace probleme
{
    public class RezolvaProblema
    {
        static readonly string[] delimitatoare = new[] { " ", ",", ".", "!", "?", ";", ":", "'" };

        public static bool EsteDelimitator(string del)
        {
            for (int i = 0; i < delimitatoare.Length; i++)
            {
                if (del == delimitatoare[i])
                {
                    return true;
                }
            }
            return false;
        }

        public static void Problema(int nr)
        {
            switch (nr)
            {
                case 1:
                    Problema1.Rezolva(10);
                    break;
                case 2:
                    Problema2.Rezolva(10);
                    break;
                case 3:
                    Problema3.Rezolva(10);
                    break;
                case 4:
                    Problema4.Rezolva();
                    break;
                case 5:
                    Problema5.Rezolva();
                    break;
                case 6:
                    Problema6.Rezolva();
                    break;
                case 7:
                    Problema7.Rezolva();
                    break;
                case 8:
                    Problema8.Rezolva();
                    break;
                case 9:
                    Problema9.Rezolva();
                    break;
                case 10:
                    Problema10.Rezolva();
                    break;
                default:
                    Console.WriteLine("Numarul problemei nu este corect");
                    break;
            }
        }

        public static void Scrie(int[] M)
        {
            for (int a = 0; a < M.Length; a++)
            {
                Console.Write(string.Format("{0}, ",M[a]));
            }
            Console.WriteLine();
        }

        public static int[] GenereazaOMatricie(int marime)
        {
            Random rnd = new Random();
            int[] M = new int[marime];
            for (int a = 0; a < marime; a++)
            {
                M[a] = rnd.Next(0, 100);
            }
            return M;
        }

        public static string[] GasesteCuvinte(string fraz)
        {
            string[] cuvinte = new string[100];
            int lungime = 0;
            string cuvint = null;
            for (int a = 0; a < fraz.Length; a++)
            {
                if (EsteDelimitator(fraz.Substring(a, 1)))
                {
                    if (cuvint != null)
                    {
                        lungime++;
                        cuvinte[lungime - 1] = cuvint;
                    }
                    cuvint = null;
                }
                else
                {
                    cuvint += fraz.Substring(a, 1);
                    if (a + 1 == fraz.Length)
                    {
                        lungime++;
                        cuvinte[lungime - 1] = cuvint;
                    }
                }
            }
            return cuvinte;
        }

        public static int PrimesteUnInt(string fraza)
        {
            while(true)
            {
                Console.WriteLine(fraza);
                string xstring = Console.ReadLine();
                bool seconverteaza = Int32.TryParse(xstring, out int x);
                if (seconverteaza == true)
                {
                    return x;
                }
            }
        }
    }
}
