﻿using System;
using System.Collections.Generic;
using System.Text;

namespace probleme.probleme
{
    public class Problema3
    {
        public static void Rezolva(int marime)
        {
            int[] matrice = RezolvaProblema.GenereazaOMatricie(marime);
            Console.WriteLine("Generam o matricie:");
            RezolvaProblema.Scrie(matrice);
            InverseazaMatricea(matrice);
            Console.WriteLine("Matricia inversata:");
            RezolvaProblema.Scrie(matrice);
        }
        public static void InverseazaMatricea(int[] matrice)
        {
            int jumatate = matrice.Length / 2;
            for (int i = 0; i < jumatate; i++)
            {
                int c = matrice[i];
                matrice[i] = matrice[matrice.Length - i - 1];
                matrice[matrice.Length - i - 1] = c;
            }
        }
    }
}
