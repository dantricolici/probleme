﻿using System;
using System.Collections.Generic;
using System.Text;

namespace probleme.probleme
{
    public class Problema8
    {
        public static void Rezolva()
        {
            Console.WriteLine("Introduceti o fraza, propozitie sau cuvinte, si ea o sa le corecteze:");
            string fraz = Console.ReadLine();
            string Fraz = PuneLitereMari(fraz);
            Console.WriteLine("fraza corectata:");
            Console.WriteLine(Fraz);
        }

        public static string PuneLitereMari(string fraz)
        {
            string Fraz = "";
            bool bul = false;
            for (int a = 0; a < fraz.Length; a++)
            {
                if (!RezolvaProblema.EsteDelimitator(fraz.Substring(a, 1)))
                {
                    if (bul == false)
                    {
                        Fraz += fraz.Substring(a, 1).ToUpper();
                        bul = true;
                    }
                    else
                    {
                        Fraz += fraz.Substring(a, 1);
                    }
                }
                else
                {
                    bul = false;
                    Fraz += fraz.Substring(a, 1);
                }
            } return Fraz;
        }
    }
}
