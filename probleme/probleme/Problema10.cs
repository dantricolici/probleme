﻿using System;

namespace probleme.probleme
{
    public class Problema10
    {
        public static void Rezolva()
        {
            int x = RezolvaProblema.PrimesteUnInt("Scrieti Xmarimea la matricea 2D");
            int y = RezolvaProblema.PrimesteUnInt("Scrieti Ymarimea la matricea 2D");
            int[,] Matrice = GenereazaO2DMatricie(y, x);
            Scrie2DMatrice(x, Matrice);
            OrdoneazaMatricia2D(x, Matrice, true);
            Scrie2DMatrice(x, Matrice);
            OrdoneazaMatricia2D(x, Matrice, false);
            Scrie2DMatrice(x, Matrice);
            for (byte a = 0; a < 3; a++)
            {
                SwapMatrice(x, Matrice, a);
                Scrie2DMatrice(x, Matrice);
            }

        }

        public static int[,] GenereazaO2DMatricie(int y, int x)
        {
            int[,] Matrice = new int[y, x];
            Random rnd = new Random();
            for (int a = 0; a < y; a++)
            {
                for (int b = 0; b < x; b++)
                {
                    Matrice[a, b] = rnd.Next(0, 10);
                }
            }
            return Matrice;
        }

        public static void Scrie2DMatrice(int x, int[,] Matrice)
        {
            for (int a = 0; a < Matrice.Length / x; a++)
            {
                for (int b = 0; b < x; b++)
                {
                    Console.Write(string.Format("{0}, ", Matrice[a, b]));
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        public static void OrdoneazaMatricia2D(int Xmax, int[,] Matrice, bool Crescator)
        {
            bool sortat = false;
            int Ymax = Matrice.Length / Xmax;

            while (sortat == false)
            {
                sortat = true;

                for (int a = 0; a < Ymax; a++)
                {
                    for (int b = 0; b < Xmax; b++)
                    {
                        if ((b + 1 == Xmax) && (a + 1 != Ymax))
                        {
                            if ((Crescator && (Matrice[a + 1, 0] < Matrice[a, b])) || (!Crescator && (Matrice[a + 1, 0] > Matrice[a, b])))
                            {
                                int c = Matrice[a + 1, 0];
                                Matrice[a + 1, 0] = Matrice[a, b];
                                Matrice[a, b] = c;
                                sortat = false;
                            }
                        }
                        if (b != Xmax - 1)
                        {
                            if ((Crescator && (Matrice[a, b + 1] < Matrice[a, b])) || (!Crescator && (Matrice[a, b + 1] > Matrice[a, b])))
                            {
                                int c = Matrice[a, b];
                                Matrice[a, b] = Matrice[a, b + 1];
                                Matrice[a, b + 1] = c;
                                sortat = false;
                            }
                        }
                    }
                }
            }
        }   
        public static void SwapMatrice(int Xmax, int[,] Matrice, byte IdSwap)
        {
            int Ymax = Matrice.Length / Xmax;
            switch(IdSwap)
            {
                case 0:
                    for (int a = 0; a < Ymax; a++)
                    {
                        for(int b = 0; b < Xmax / 2; b++)
                        {
                            int c = Matrice[a, b];
                            Matrice[a, b] = Matrice[a, Xmax - 1 - b];
                            Matrice[a, Xmax - 1 - b] = c;
                        }
                    }
                    break;
                case 1:
                    for (int a = 0; a < Xmax; a++)
                    {
                        for (int b = 0; b < Ymax / 2; b++)
                        {
                            int c = Matrice[b, a];
                            Matrice[b, a] = Matrice[Ymax - 1 - b, a];
                            Matrice[Ymax - 1 - b, a] = c;
                        }
                    }
                    break;
                case 2:
                    Console.WriteLine("SORRY BUT PROGRAM DONT KNOW HOW TO SWAP 'Matrice' DIAGONAL");
                    break;
            }
        }
    }
}
