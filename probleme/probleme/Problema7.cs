﻿using System;
using System.Collections.Generic;
using System.Text;

namespace probleme.probleme
{
    class Problema7
    {
        public static void Rezolva()
        {
            Console.WriteLine("Introduceti o fraza, un cuvint, sau o propozitie si programa va gasi cuvintele.");
            string fraz = Console.ReadLine();
            string[] cuvinte = RezolvaProblema.GasesteCuvinte(fraz);
            for (int b = 1; b < cuvinte.Length; b++)
            {
                if (cuvinte[b - 1] == null)
                {
                    break;
                }
                else
                {
                    Console.WriteLine(string.Format("{0}. {1}", b, cuvinte[b - 1]));
                }
            }
        }
    }
}
