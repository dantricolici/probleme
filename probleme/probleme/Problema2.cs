﻿using System;

namespace probleme.probleme
{
    public class Problema2
    {
        public static void Rezolva(int marime)
        {
            Console.WriteLine("initial array:");
            int[] matrice = RezolvaProblema.GenereazaOMatricie(marime);
            RezolvaProblema.Scrie(matrice);

            int Jumatate = matrice.Length / 2;
            OrdoneazaMatricia(true, 0, Jumatate - 1, matrice); 
            OrdoneazaMatricia(false, Jumatate, matrice.Length - 1, matrice);

            Console.WriteLine("sorted array:");
            RezolvaProblema.Scrie(matrice);
        }
        public static void OrdoneazaMatricia(bool crescator, int deLa, int panaLa, int[] matricia)
        {
            bool sortat = false;

            while (!sortat)
            {
                sortat = true;

                for (int i = deLa; i < panaLa; i++)
                {
                    if ((crescator && (matricia[i] > matricia[i + 1])) || (!crescator && (matricia[i] < matricia[i + 1])))
                    {
                        int tmp = matricia[i];
                        matricia[i] = matricia[i + 1];
                        matricia[i + 1] = tmp;
                        sortat = false;
                    }
                }
            }
        }
    }
}
