﻿using System;

namespace probleme.probleme
{
    public class Problema1
    {
        public static void Rezolva(int marime)
        {
            int[] matrice = RezolvaProblema.GenereazaOMatricie(marime);
            Console.WriteLine("generam o matricie:");
            RezolvaProblema.Scrie(matrice);
            SorteazaCrescator(matrice);
            Console.WriteLine("Matricia ordonata crescator:");
            RezolvaProblema.Scrie(matrice);
        }

        public static void SorteazaCrescator(int[] matrice)
        {
            bool esteSortat = false;

            while (!esteSortat)
            {
                esteSortat = true;
                for (int b = 1; b < matrice.Length; b++)
                {
                    if (matrice[b - 1] > matrice[b])
                    {
                        int c = matrice[b];
                        matrice[b] = matrice[b - 1];
                        matrice[b - 1] = c;
                        esteSortat = false;
                    }
                }
            }
        }
    }
}
