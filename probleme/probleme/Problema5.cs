﻿using System;

namespace probleme.probleme
{
    public class Problema5
    {
        public static bool EsteXPrim(int x)
        {
            int i = 2;
            bool estexprim;
            while (true)
            {
                if (x % i == 0)
                {
                    if (i != x)
                    {
                        estexprim = false;
                        break;
                    }
                    else
                    {
                        estexprim = true;
                        break;
                    }
                }
                i++;
            }
            return estexprim;
        }
        public static void Rezolva()
        {
            int CiteNumere = RezolvaProblema.PrimesteUnInt("cite numere prime vreai ca eu sa gasesc:");
            int i = 2;
            Console.WriteLine("numerele prime: ");
            for (int a = 0; a < CiteNumere; i++)
            {
                if (EsteXPrim(i))
                {
                    a++;
                    Console.Write(string.Format("{0}, ", i));
                }
            }
        }
    }
}
