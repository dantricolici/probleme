﻿using System;
using System.Collections.Generic;
using System.Text;

namespace probleme.probleme
{
    public class Problema9
    {
        public static void ScrieString(string[] cuv, string[] del)
        {
            int cuvm = 0;
            for (int a = 0; a < del.Length; a++)
            {
                if (del[a] != null)
                {
                    Console.Write(del[a]);
                }
                else
                {
                    Console.Write(cuv[cuvm]);
                    cuvm++;
                }
            }
            Console.WriteLine();
        }
        public static string[] SortareaCuvintelor(string[] cuvinte, byte crescatorID, int marime)
        {
            bool crescator = false;
            if (crescatorID == 0)
            {
                crescator = true;
            }
            if ((crescatorID == 0) || (crescatorID == 1))
            {
                bool gata = false;
                while (!gata)
                {
                    gata = true;
                    for (int a = 0; a < marime - 1; a++)
                    {
                        if ((crescator && (cuvinte[a].Length > cuvinte[a + 1].Length)) || (!crescator && (cuvinte[a].Length < cuvinte[a + 1].Length)))
                        {
                            string caca = cuvinte[a];
                            cuvinte[a] = cuvinte[a + 1];
                            cuvinte[a + 1] = caca;
                            gata = false;
                        }
                    }
                }
            } else
            {
                Random rnd = new Random();
                bool gata = false;
                while (!gata)
                {
                    gata = true;
                    for (int a = 0; a < marime; a++)
                    {
                        int random = rnd.Next(0, 2);
                        int r = rnd.Next(0, 2);
                        if (random == r)
                        {
                            string caca = cuvinte[a];
                            cuvinte[a] = cuvinte[a + 1];
                            cuvinte[a + 1] = caca;
                            gata = false;
                        }
                    }
                }
            }
            return cuvinte;
        }
        public static void Rezolva()
        {
            Console.WriteLine("Introduceti o propozitie sau o fraza si programa va ordona cuvintele:");
            string fraz = Console.ReadLine();
            string[] deli = new string[100];
            string[] cuvinte = RezolvaProblema.GasesteCuvinte(fraz);
            bool bul = false;
            int b = 0;
            for (int a = 0; a < fraz.Length; a++)
            {
                if (RezolvaProblema.EsteDelimitator(fraz.Substring(a, 1)))
                {
                    deli[b] = fraz.Substring(a, 1);
                    bul = false;
                    b++;
                }
                else
                {
                    if (bul == false)
                    {
                        deli[b] = null;
                        bul = true;
                        b++;
                    }
                }
            }
            int marime = 0;
            for (int i = 0; i < cuvinte.Length; i++)
            {
                if (cuvinte[i] == null)
                {
                    marime = i;
                    break;
                }
            }
            cuvinte = SortareaCuvintelor(cuvinte, 0, marime);
            Console.WriteLine("Propozitia ordonata crescator:");
            ScrieString(cuvinte, deli);
            cuvinte = SortareaCuvintelor(cuvinte, 1, marime);
            Console.WriteLine("Propozitia ordonata descrescator:");
            ScrieString(cuvinte, deli);
            cuvinte = SortareaCuvintelor(cuvinte, 2, marime);
            Console.WriteLine("Propozitia ordonata random:");
            ScrieString(cuvinte, deli);
        }
    }
}
