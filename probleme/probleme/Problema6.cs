﻿using System;

namespace probleme.probleme
{
    public class Problema6
    {
        public static void Rezolva()
        {
            Console.WriteLine("Scrieti o fraza, propozitie sau cuvint si ea va fi inversata:");
            string fraz = Console.ReadLine();
            string zraf = InverseazaPropozitia(fraz);
            Console.WriteLine("Fraza inversata:");
            Console.WriteLine(zraf);
        }

        public static string InverseazaPropozitia(string propozitia)
        {
            string aitizoporp = "";
            for (int a = propozitia.Length; a > 0; a--)
            {
                aitizoporp += propozitia.Substring(a - 1, 1);
            }
            return aitizoporp;
        }
    }
}
