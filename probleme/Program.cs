﻿using System;

namespace probleme
{
    class Program
    {
        private static readonly string[] menuItems = new string[] 
            {"Problema 1", "Problema 2", "Problema 3", "Problema 4", "Problema 5", 
              "Problema 6", "Problema 7", "Problema 8", "Problema 9", "Problema 10", "Exit"};

        private static void SetColorToDefaults()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
        }

        private static void ShowMenu(int selected)
        {
            int w = Console.WindowWidth;
            for (int a = 0; a < menuItems.Length; a++)
            {
                SetColorToDefaults();
                if (a == selected)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.BackgroundColor = ConsoleColor.DarkBlue;
                    Console.SetCursorPosition(w / 2 - menuItems[a].Length / 2, a);
                    Console.Write(menuItems[a]);
                    SetColorToDefaults();
                } else
                {
                    Console.SetCursorPosition(w / 2 - menuItems[a].Length / 2, a);
                    Console.Write(menuItems[a]);
                }
            }
        }

        private static void Execute(int selectat)
        {
            Console.Clear();
            if(selectat == menuItems.Length - 1)
            {
                Environment.Exit(0);
            } else
            {
                RezolvaProblema.Problema(selectat + 1);
                Console.ReadKey();
                Console.Clear();
            }
        }

        static void Main()
        {
            Console.SetWindowSize(160, 45);
            int selectat = 0;
            while (true)
            {
                ShowMenu(selectat);
                var key = Console.ReadKey().Key;
                switch(key)
                {
                    case ConsoleKey.W:
                    case ConsoleKey.UpArrow:
                        if (selectat != 0)
                        {
                            selectat--;
                        }
                        break;
                    case ConsoleKey.S:
                    case ConsoleKey.DownArrow:
                        if (selectat != menuItems.Length - 1)
                        {
                            selectat++;
                        }
                        break;
                    case ConsoleKey.Enter:
                        Execute(selectat);
                        break;
                }
            }
        }
    }
}
